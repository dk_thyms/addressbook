package org.dk.services;

import org.dk.BaseTest;
import org.dk.models.Entry;
import org.dk.models.EntryBuilder;
import org.dk.services.criterias.Criteria;
import org.dk.types.Gender;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AddressBookServiceTest extends BaseTest {

    private AddressBookService addressBookService;
    private EntryBuilder entryBuilder;

    @Before
    public void setup() throws Exception {
        addressBookService = new AddressBookService();
        entryBuilder = new EntryBuilder("James Holmes", Gender.FEMALE, "01/01/85");
    }

    @Test
    public void addEntry_should_add_entry_to_address_book() throws Exception {
        // expect
        assertEquals(0, addressBookService.size());

        // when
        addressBookService.addEntry(entryBuilder.withGender(Gender.MALE).build());

        // then
        assertEquals(1, addressBookService.size());

        // when
        addressBookService.addEntry(entryBuilder.withGender(Gender.FEMALE).build());

        // then
        assertEquals(2, addressBookService.size());
    }

    @Test
    public void findBy_should_return_entries_from_given_criteria_query() throws Exception {
        // given
        Criteria criteriaMock = mock(Criteria.class);
        List<Entry> entriesExpected = new ArrayList<Entry>();
        when(criteriaMock.query(Matchers.anyListOf(Entry.class))).thenReturn(entriesExpected);

        // when
        List<Entry> entries = addressBookService.findBy(criteriaMock);

        // then
        assertEquals(entriesExpected, entries);
    }
}