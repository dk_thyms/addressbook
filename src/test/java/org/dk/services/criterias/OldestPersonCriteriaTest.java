package org.dk.services.criterias;

import org.dk.BaseTest;
import org.dk.models.Entry;
import org.dk.models.EntryBuilder;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class OldestPersonCriteriaTest extends BaseTest {

  @Test
  public void should_return_empty_list_if_no_matching_for_given_gender() throws Exception {
    // given
    Criteria criteria = new OldestPersonCriteria();
    List<Entry> entries = new ArrayList<Entry>();

    // when
    List<Entry> resultEntries = criteria.query(entries);

    // then
    assertNotNull(resultEntries);
    assertTrue(resultEntries.isEmpty());
  }

  @Test
  public void should_return_the_entry_for_oldest_person() throws Exception {
    // given
    Criteria criteria = new OldestPersonCriteria();
    List<Entry> entries = new ArrayList<Entry>();

    EntryBuilder entryBuilder = new EntryBuilder();
    entries.add(entryBuilder.withDateOfBirth("08/03/90").build());
    entries.add(entryBuilder.withDateOfBirth("01/01/77").build());
    entries.add(entryBuilder.withDateOfBirth("02/08/85").build());

    // when
    List<Entry> resultEntries = criteria.query(entries);

    // then
    assertEquals("01/01/77", simpleDateFormat.format(resultEntries.get(0).getDateOfBirth()));

    // when
    entries.add(entryBuilder.withDateOfBirth("03/08/71").build());
    resultEntries = criteria.query(entries);

    // then
    assertEquals("03/08/71", simpleDateFormat.format(resultEntries.get(0).getDateOfBirth()));
  }

}