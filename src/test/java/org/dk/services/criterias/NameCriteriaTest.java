package org.dk.services.criterias;

import org.dk.BaseTest;
import org.dk.models.Entry;
import org.dk.models.EntryBuilder;
import org.dk.types.Gender;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class NameCriteriaTest extends BaseTest {

  @Test
  public void should_return_empty_list_if_no_matching_for_given_name() throws Exception {
    // given
    Criteria criteria = new NameCriteria("James", "Holmes");
    List<Entry> entries = new ArrayList<Entry>();

    // when
    List<Entry> resultEntries = criteria.query(entries);

    // then
    assertNotNull(resultEntries);
    assertTrue(resultEntries.isEmpty());
  }

  @Test
  public void findEntryByName_should_return_entry_for_given_first_and_last_name() throws Exception {
    // given
    Criteria criteria = new NameCriteria("James", "Holmes");
    List<Entry> entries = new ArrayList<Entry>();

    EntryBuilder entryBuilder = new EntryBuilder("David White", Gender.MALE, "01/01/71");
    entries.add(entryBuilder.withName("Katie Brown").build());
    entries.add(entryBuilder.withName("James Holmes").build());
    entries.add(entryBuilder.withName("Kim Lee").build());

    // when
    List<Entry> resultEntries = criteria.query(entries);

    // then
    assertEquals("James", resultEntries.get(0).getFirstName());
    assertEquals("Holmes", resultEntries.get(0).getLastName());
  }

}