package org.dk.services.criterias;

import org.dk.BaseTest;
import org.dk.models.Entry;
import org.dk.models.EntryBuilder;
import org.dk.types.Gender;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class GenderCriteriaTest extends BaseTest {

  @Test
  public void should_return_empty_list_if_no_matching_for_given_gender() throws Exception {
    // given
    Criteria criteria = new GenderCriteria(Gender.FEMALE);
    List<Entry> entries = new ArrayList<Entry>();

    // when
    List<Entry> resultEntries = criteria.query(entries);

    // then
    assertNotNull(resultEntries);
    assertTrue(resultEntries.isEmpty());
  }

  @Test
  public void should_return_entries_for_given_gender() throws Exception {
    // given
    Criteria criteria = new GenderCriteria(Gender.FEMALE);
    List<Entry> entries = new ArrayList<Entry>();

    EntryBuilder entryBuilder = new EntryBuilder("James Holmes", Gender.MALE, "01/01/77");
    entries.add(entryBuilder.withGender(Gender.FEMALE).build());
    entries.add(entryBuilder.withGender(Gender.MALE).build());
    entries.add(entryBuilder.withGender(Gender.FEMALE).build());

    // when
    List<Entry> resultEntries = criteria.query(entries);

    // then
    assertEquals(2, resultEntries.size());
  }
}