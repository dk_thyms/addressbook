package org.dk.services;

import org.dk.BaseTest;
import org.dk.models.Entry;
import org.dk.types.Gender;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FileAddressBookLoaderServiceTest extends BaseTest {

  @Test
  public void should_load_address_book_entries_from_given_file() throws Exception {
    // given
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
    final List<Entry> entries = new ArrayList<Entry>();

    AddressBookLoaderService addressBookLoader = new FileAddressBookLoaderService("AddressBook");
    AddressBookService addressBookService = new AddressBookService() {
      public void addEntry(Entry entry) {
        entries.add(entry);
      }
    };

    // when
    addressBookLoader.load(addressBookService);

    // then
    assertEquals(5, entries.size());
    assertEquals("Bill", entries.get(0).getFirstName());
    assertEquals("McKnight", entries.get(0).getLastName());
    assertEquals(Gender.MALE, entries.get(0).getGender());
    assertEquals("16/03/77", sdf.format(entries.get(0).getDateOfBirth()));

    // and
    assertEquals("Paul", entries.get(1).getFirstName());
    assertEquals("Gemma", entries.get(2).getFirstName());
    assertEquals("Sarah", entries.get(3).getFirstName());
    assertEquals("Wes", entries.get(4).getFirstName());
  }
}
