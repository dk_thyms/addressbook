package org.dk.models;

import org.dk.BaseTest;
import org.dk.types.Gender;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;

import static org.junit.Assert.assertEquals;

public class EntryBuilderTest extends BaseTest {

  private EntryBuilder entryBuilder;

  @Before
  public void setup() {
    entryBuilder = new EntryBuilder("James Holmes", Gender.FEMALE, "01/01/85");
  }

  @Test
  public void should_build_entry_with_default() throws Exception {
    // when
    Entry entry = entryBuilder.build();

    // then
    assertEquals(Gender.FEMALE, entry.getGender());
    assertEquals("01/01/85", simpleDateFormat.format(entry.getDateOfBirth()));
    assertEquals("James", entry.getFirstName());
    assertEquals("Holmes", entry.getLastName());
  }

  @Test
  public void should_build_entry_with_given_gender() throws Exception {
    // when
    Entry entry = entryBuilder.withGender(Gender.MALE).build();

    // then
    assertEquals(entry.getGender(), Gender.MALE);
  }

  @Test
  public void should_build_entry_with_given_date_of_birth() throws Exception {
    // when
    Entry entry = entryBuilder.withDateOfBirth("02/03/77").build();

    // then
    assertEquals("02/03/77", simpleDateFormat.format(entry.getDateOfBirth()));
  }

  @Test(expected = ParseException.class)
  public void should_throw_parse_exception_for_invalid_date() throws Exception{
    // expect
    entryBuilder.withDateOfBirth("02/03/yy").build();
  }
}