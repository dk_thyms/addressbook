package org.dk.types;

import org.dk.BaseTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GenderTest extends BaseTest {

  @Test
  public void fromValue_should_return_gender_type_from_given_value() throws Exception {
    // expect
    assertEquals(Gender.MALE, Gender.fromValue("Male"));
    assertEquals(Gender.FEMALE, Gender.fromValue("Female"));
  }

}