package org.dk.utils;

import org.dk.BaseTest;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;

public class DateUtilsTest extends BaseTest {

  @Test
  public void daysBetween_should_return_number_of_days_between_two_date() throws Exception {
    // given
    Date date1 = simpleDateFormat.parse("01/02/77");
    Date date2 = simpleDateFormat.parse("05/02/77");

    // when
    int actualNumberOfDaysBetween = DateUtils.daysBetween(date1, date2);

    // then
    assertEquals(4, actualNumberOfDaysBetween);
  }
}
