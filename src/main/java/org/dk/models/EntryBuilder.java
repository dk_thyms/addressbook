package org.dk.models;

import org.dk.types.Gender;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class EntryBuilder {
  private String firstName;
  private String lastName;
  private Gender gender;
  private String dateOfBirth;

  private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");

  public EntryBuilder() {}

  public EntryBuilder(String name, Gender defaultGender, String dateOfBirth) {
    setNames(name);
    this.gender = defaultGender;
    this.dateOfBirth = dateOfBirth;
  }

  public EntryBuilder withName(String name) {
    setNames(name);

    return this;
  }

  public EntryBuilder withGender(Gender gender) {
    this.gender = gender;

    return this;
  }

  public EntryBuilder withDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;

    return this;
  }

  public Entry build() throws ParseException {
    return new Entry(firstName, lastName, gender, sdf.parse(dateOfBirth));
  }

  private void setNames(String name) {
    String[] names = name.split(" ");
    this.firstName = names[0];
    this.lastName = names[1];
  }
}
