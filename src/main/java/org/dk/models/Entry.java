package org.dk.models;

import org.dk.types.Gender;

import java.util.Date;

public class Entry {
  private Gender gender;
  private Date dateOfBirth;
  private String firstName;
  private String lastName;

  public Entry(String firstName, String lastName, Gender gender, Date dateOfBirth) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.gender = gender;
    this.dateOfBirth = dateOfBirth;
  }

  public Gender getGender() {
    return gender;
  }

  public Date getDateOfBirth() {
    return dateOfBirth;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }
}
