package org.dk.services;

public interface AddressBookLoaderService {

  void load(AddressBookService addressBookService) throws Exception;

}
