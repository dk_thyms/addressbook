package org.dk.services.criterias;

import org.dk.models.Entry;
import org.dk.types.Gender;

import java.util.ArrayList;
import java.util.List;

public class GenderCriteria implements Criteria {
  private Gender gender;

  public GenderCriteria(Gender gender) {
    this.gender = gender;
  }

  @Override
  public List<Entry> query(List<Entry> entries) {
    List<Entry> entriesByGender = new ArrayList<Entry>();
    for(Entry entry : entries) {
      if (entry.getGender() == gender) {
        entriesByGender.add(entry);
      }
    }

    return entriesByGender;
  }
}
