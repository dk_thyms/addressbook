package org.dk.services.criterias;

import org.dk.models.Entry;

import java.util.List;

public interface Criteria {

  List<Entry> query(List<Entry> entries);

}
