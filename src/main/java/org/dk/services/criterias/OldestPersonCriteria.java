package org.dk.services.criterias;

import org.dk.models.Entry;

import java.util.ArrayList;
import java.util.List;

public class OldestPersonCriteria implements Criteria {

  @Override
  public List<Entry> query(List<Entry> entries) {
    List<Entry> entriesForOldestPerson = new ArrayList<Entry>();

    if (!entries.isEmpty()) {
      Entry entryForOldestPerson = entries.get(0);
      for (int i = 1; i < entries.size(); i++) {
        Entry currentEntry = entries.get(i);
        if (currentEntry.getDateOfBirth().before(entryForOldestPerson.getDateOfBirth())) {
          entryForOldestPerson = currentEntry;
        }
      }

      if (entryForOldestPerson != null) {
        entriesForOldestPerson.add(entryForOldestPerson);
      }
    }

    return entriesForOldestPerson;
  }

}
