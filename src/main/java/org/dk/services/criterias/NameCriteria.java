package org.dk.services.criterias;

import org.dk.models.Entry;

import java.util.ArrayList;
import java.util.List;

public class NameCriteria implements Criteria {
  private String firstName;
  private String lastName;

  public NameCriteria(String firstName, String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  @Override
  public List<Entry> query(List<Entry> entries) {
    List<Entry> entriesForName = new ArrayList<Entry>();

    Entry entryByName = null;
    for (Entry entry : entries) {
      if (entry.getFirstName().equalsIgnoreCase(firstName) && entry.getLastName().equalsIgnoreCase(lastName)) {
        entryByName = entry;
      }
    }

    if (entryByName != null) {
      entriesForName.add(entryByName);
    }

    return entriesForName;
  }
}
