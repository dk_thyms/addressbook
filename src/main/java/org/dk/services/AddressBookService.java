package org.dk.services;

import org.dk.services.criterias.Criteria;
import org.dk.models.Entry;

import java.util.ArrayList;
import java.util.List;

public class AddressBookService {
  private List<Entry> entries = new ArrayList<Entry>();

  public int size() {
    return entries.size();
  }

  public void addEntry(Entry entry) {
    entries.add(entry);
  }

  public List<Entry> findBy(Criteria criteria) {
    return criteria.query(entries);
  }
}
