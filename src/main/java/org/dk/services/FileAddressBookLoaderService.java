package org.dk.services;

import org.dk.models.Entry;
import org.dk.models.EntryBuilder;
import org.dk.types.Gender;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FileAddressBookLoaderService implements AddressBookLoaderService {

  private String filename;

  public FileAddressBookLoaderService(String filename) {
    this.filename = filename;
  }

  @Override
  public void load(AddressBookService addressBookService) throws Exception {
    InputStream inputStream = Thread.currentThread().getClass().getResourceAsStream("/" + filename);
    InputStreamReader reader = new InputStreamReader(inputStream, "UTF-8");
    BufferedReader br = new BufferedReader(reader);

    String strLine;
    EntryBuilder entryBuilder = new EntryBuilder();
    while ((strLine = br.readLine()) != null)   {
      String[] parts = strLine.split(", ");

      Entry entry = entryBuilder.withName(parts[0])
                      .withGender(Gender.fromValue(parts[1]))
                      .withDateOfBirth(parts[2])
                      .build();
      addressBookService.addEntry(entry);
    }
    br.close();
  }

}
