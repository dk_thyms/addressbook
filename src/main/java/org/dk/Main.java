package org.dk;

import org.dk.models.Entry;
import org.dk.services.AddressBookLoaderService;
import org.dk.services.AddressBookService;
import org.dk.services.FileAddressBookLoaderService;
import org.dk.services.criterias.Criteria;
import org.dk.services.criterias.GenderCriteria;
import org.dk.services.criterias.NameCriteria;
import org.dk.services.criterias.OldestPersonCriteria;
import org.dk.types.Gender;
import org.dk.utils.DateUtils;

import java.util.List;

public class Main {

  public static void main(String[] args) throws Exception {
    AddressBookService addressBookService = new AddressBookService();
    AddressBookLoaderService addressBookLoaderService = new FileAddressBookLoaderService("AddressBook");
    addressBookLoaderService.load(addressBookService);

    Criteria criteria = new GenderCriteria(Gender.MALE);
    List<Entry> entriesForMale = addressBookService.findBy(criteria);
    System.out.print("How many males are in the address book?\t");
    System.out.println("Answer: " + entriesForMale.size());

    criteria = new OldestPersonCriteria();
    List<Entry> entries = addressBookService.findBy(criteria);
    Entry entryForOldestPerson = entries.get(0);
    System.out.print("Who is the oldest person in the address book?\t");
    System.out.println("Answer: " + entryForOldestPerson.getFirstName() + " " + entryForOldestPerson.getLastName());

    criteria = new NameCriteria("Bill", "McKnight");
    Entry entryForBill = addressBookService.findBy(criteria).get(0);
    criteria = new NameCriteria("Paul", "Robinson");
    Entry entryForPaul = addressBookService.findBy(criteria).get(0);
    System.out.print("How many days older is Bill than Paul?\t");
    System.out.println("Answer: " + DateUtils.daysBetween(entryForBill.getDateOfBirth(), entryForPaul.getDateOfBirth()) + " days");
  }

}
