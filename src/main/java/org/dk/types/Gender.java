package org.dk.types;

public enum Gender {
  FEMALE, MALE;

  public static Gender fromValue(String value) {
    Gender genderFound = null;

    for (Gender gender : Gender.values()) {
      if (gender.name().equalsIgnoreCase(value)) {
        genderFound = gender;
      }
    }

    return genderFound;
  }
}
